package clonecalculator;

import javax.swing.*;
import java.awt.*;

public class CloneCalculator {

    public CloneCalculator() {
        JPanel cal = new JPanel(new BorderLayout());

        Font font1 = new Font("Arial", Font.BOLD, 20);
        JTextArea txt1 = new JTextArea("", 1, 2);
        txt1.setBackground(Color.darkGray);
        txt1.setForeground(Color.gray);
        txt1.setFont(font1);
        txt1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        Font font2 = new Font("Arial", Font.BOLD, 55);
        JTextArea txt2 = new JTextArea("0", 1, 2);
        txt2.setBackground(Color.darkGray);
        txt2.setForeground(Color.WHITE);
        txt2.setFont(font2);
        txt2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        cal.add(txt1, BorderLayout.NORTH);
        cal.add(txt2, BorderLayout.CENTER);

        JPanel sixbts = new JPanel(new GridLayout(1, 6, 2, 2));
        sixbts.setPreferredSize(new Dimension(320, 30));
        sixbts.setBackground(Color.darkGray);
        
        JButton bt_MC = new JButton("MC");
        bt_MC.setBackground(Color.darkGray);
        bt_MC.setForeground(Color.lightGray);
        bt_MC.setBorder(null);
        sixbts.add(bt_MC);

        JButton bt_MR = new JButton("MR");
        bt_MR.setBackground(Color.darkGray);
        bt_MR.setForeground(Color.lightGray);
        bt_MR.setBorder(null);
        sixbts.add(bt_MR);
        
        JButton bt_Mplus = new JButton("M+");
        bt_Mplus.setBackground(Color.darkGray);
        bt_Mplus.setForeground(Color.white);
        bt_Mplus.setBorder(null);
        sixbts.add(bt_Mplus);
        
        JButton bt_Msub = new JButton("M-");
        bt_Msub.setBackground(Color.darkGray);
        bt_Msub.setForeground(Color.white);
        bt_Msub.setBorder(null);
        sixbts.add(bt_Msub);
        
        JButton bt_MS = new JButton("MS");
        bt_MS.setBackground(Color.darkGray);
        bt_MS.setForeground(Color.white);
        bt_MS.setBorder(null);
        sixbts.add(bt_MS);
        
        JButton bt_Mdown = new JButton("M˅");
        bt_Mdown.setBackground(Color.darkGray);
        bt_Mdown.setForeground(Color.lightGray);
        bt_Mdown.setBorder(null);
        sixbts.add(bt_Mdown);

        JPanel mainbts = new JPanel(new GridLayout(6, 4, 2, 2));
        mainbts.setPreferredSize(new Dimension(320, 300));
        mainbts.setBackground(Color.darkGray);

        JButton bt_percent = new JButton("％");
        bt_percent.setBackground(Color.gray);
        bt_percent.setForeground(Color.white);
        bt_percent.setBorder(null);
        mainbts.add(bt_percent);
        
         JButton bt_CE = new JButton("CE");
        bt_CE.setBackground(Color.gray);
        bt_CE.setForeground(Color.white);
        bt_CE.setBorder(null);
        mainbts.add(bt_CE);
        
        JButton bt_C = new JButton("C");
        bt_C.setBackground(Color.gray);
        bt_C.setForeground(Color.white);
        bt_C.setBorder(null);
        mainbts.add(bt_C);
        
        JButton bt_backspace = new JButton("⌫");
        bt_backspace.setBackground(Color.gray);
        bt_backspace.setForeground(Color.white);
        bt_backspace.setBorder(null);
        mainbts.add(bt_backspace);

        JButton bt_1dvx = new JButton("1/x");
        bt_1dvx.setBackground(Color.gray);
        bt_1dvx.setForeground(Color.white);
        bt_1dvx.setBorder(null);
        mainbts.add(bt_1dvx);
        
        JButton bt_square = new JButton("x²");
        bt_square.setBackground(Color.gray);
        bt_square.setForeground(Color.white);
        bt_square.setBorder(null);
        mainbts.add(bt_square);
        
        JButton bt_sqrt = new JButton("√x");
        bt_sqrt.setBackground(Color.gray);
        bt_sqrt.setForeground(Color.white);
        bt_sqrt.setBorder(null);
        mainbts.add(bt_sqrt);
        
        JButton bt_divine = new JButton("÷");
        bt_divine.setBackground(Color.gray);
        bt_divine.setForeground(Color.white);
        bt_divine.setBorder(null);
        mainbts.add(bt_divine);

        JButton bt_7 = new JButton("7");
        bt_7.setBackground(Color.gray);
        bt_7.setForeground(Color.white);
        bt_7.setBorder(null);
        mainbts.add(bt_7);
        
        JButton bt_8 = new JButton("8");
        bt_8.setBackground(Color.gray);
        bt_8.setForeground(Color.white);
        bt_8.setBorder(null);
        mainbts.add(bt_8);
        
        JButton bt_9 = new JButton("9");
        bt_9.setBackground(Color.gray);
        bt_9.setForeground(Color.white);
        bt_9.setBorder(null);
        mainbts.add(bt_9);
        
        JButton bt_multiply = new JButton("×");
        bt_multiply.setBackground(Color.gray);
        bt_multiply.setForeground(Color.white);
        bt_multiply.setBorder(null);
        mainbts.add(bt_multiply);
        
        JButton bt_4 = new JButton("4");
        bt_4.setBackground(Color.gray);
        bt_4.setForeground(Color.white);
        bt_4.setBorder(null);
        mainbts.add(bt_4);
        
        JButton bt_5 = new JButton("5");
        bt_5.setBackground(Color.gray);
        bt_5.setForeground(Color.white);
        bt_5.setBorder(null);
        mainbts.add(bt_5);
        
        JButton bt_6 = new JButton("6");
        bt_6.setBackground(Color.gray);
        bt_6.setForeground(Color.white);
        bt_6.setBorder(null);
        mainbts.add(bt_6);
        
        JButton bt_subtract = new JButton("-");
        bt_subtract.setBackground(Color.gray);
        bt_subtract.setForeground(Color.white);
        bt_subtract.setBorder(null);
        mainbts.add(bt_subtract);

        JButton bt_1 = new JButton("1");
        bt_1.setBackground(Color.gray);
        bt_1.setForeground(Color.white);
        bt_1.setBorder(null);
        mainbts.add(bt_1);
        
        JButton bt_2 = new JButton("2");
        bt_2.setBackground(Color.gray);
        bt_2.setForeground(Color.white);
        bt_2.setBorder(null);
        mainbts.add(bt_2);
        
        JButton bt_3 = new JButton("3");
        bt_3.setBackground(Color.gray);
        bt_3.setForeground(Color.white);
        bt_3.setBorder(null);
        mainbts.add(bt_3);
        
        JButton bt_plus = new JButton("+");
        bt_plus.setBackground(Color.gray);
        bt_plus.setForeground(Color.white);
        bt_plus.setBorder(null);
        mainbts.add(bt_plus);

        JButton bt_pors = new JButton("+/-");
        bt_pors.setBackground(Color.gray);
        bt_pors.setForeground(Color.white);
        bt_pors.setBorder(null);
        mainbts.add(bt_pors);
        
        JButton bt_0 = new JButton("0");
        bt_0.setBackground(Color.gray);
        bt_0.setForeground(Color.white);
        bt_0.setBorder(null);
        mainbts.add(bt_0);
        
        JButton bt_comma = new JButton(",");
        bt_comma.setBackground(Color.gray);
        bt_comma.setForeground(Color.white);
        bt_comma.setBorder(null);
        mainbts.add(bt_comma);
        
        JButton bt_equal = new JButton("=");
        bt_equal.setBackground(Color.gray);
        bt_equal.setForeground(Color.white);
        bt_equal.setBorder(null);
        mainbts.add(bt_equal);

        JPanel buttons = new JPanel(new BorderLayout());
        buttons.add(sixbts, BorderLayout.NORTH);
        buttons.add(mainbts, BorderLayout.CENTER);

        JFrame frame = new JFrame("Calculator");
        frame.setLayout(new BorderLayout());
        frame.add(cal, BorderLayout.CENTER);
        frame.add(buttons, BorderLayout.SOUTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new CloneCalculator();
    }
}
